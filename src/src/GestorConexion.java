/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

/**
 *
 * @author Alberto
 */
public class GestorConexion 
{
    Connection connection;
    public void conectameBBDD() 
    {
        connection = null;
        try 
        {
            String url1 = "jdbc:mysql://localhost:3306/tiendaPiezas?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
            String user = "root";
            //String password = "";
            String password = "root";
            connection = (Connection)DriverManager.getConnection(url1, user, password);
            if (connection != null) 
            {
              System.out.println("Conectado a tiendaPiezas");
            }
        } 
        catch (SQLException ex) 
        {
            System.out.println("ERROR: dirección o usuario/clave no válida");
            ex.printStackTrace();
        }     
    } 
    
    public void desconectaBBDD()
    {
        try 
        {
            System.out.println("Cerrada conexion con la BBDD");
            connection.close();
        } 
        catch (SQLException ex) 
        {
            System.out.println("ERROR:al cerrar la conexión");
            ex.printStackTrace();
        }
    }
    
    //------------------------------Para las tablas-----------------------------
    public ResultSet select_Tabla(String tabla)
    {
        ResultSet rs = null;
        if(connection != null)
        {
            try 
            {
                Statement sta = connection.createStatement();
                if (tabla.equals("cliente_pieza"))
                {
                    String querycp= "SELECT cp.DNI_cliente, c.Nombre, c.Apellido, cp.ID_pieza, "
                            + "p.Nombre, cp.cantidad, cp.fecha_compra " 
                            + "FROM cliente c, pieza p, cliente_pieza cp " 
                            + "WHERE c.DNI = cp.DNI_cliente "
                            + "AND p.ID_pieza = cp.ID_pieza";
                    rs = sta.executeQuery(querycp);
                }
                else if(tabla.equals("pieza"))
                {
                    String querycp= "SELECT p.*, pp.Nombre "
                            + "FROM pieza p, proveedor pp "
                            + "WHERE p.Id_proveedor = pp.ID_proveedor"
                            + " ORDER BY p.ID_pieza ASC";
                    rs = sta.executeQuery(querycp);
                }
                else
                {
                    String query = "SELECT * FROM " + tabla;
                    rs = sta.executeQuery(query);
                }
                return rs;
            } 
            catch (SQLException ex) 
            {
                System.out.println("ERROR:al consultar");
                ex.printStackTrace();
                return rs;
            }
        }
        return rs;        
    }
    //--------------------------------------------------------------------------
    
    //------------------------Borra y buscardor de tablas-----------------------
    public void delete(String tabla, String id, String clave)
    {
        if(connection != null)
        {
            try 
            {
                Statement sta = connection.createStatement();
                sta.executeUpdate("DELETE FROM " + tabla +" WHERE " + id +" = '" 
                        + clave + "'");
                sta.close();
            } 
            catch (SQLException ex) 
            {
                System.out.println("ERROR:al insertar");
                ex.printStackTrace();
            }
        }
    }
    
    public ResultSet buscador(String tabla, String campoBuscar, 
            String palabraBuscada)
    {
        ResultSet rs = null;
        try 
        {
            if(tabla.equals("pieza"))
            {
                String query = "SELECT p.*, pp.Nombre "
                        + "FROM " + tabla + " p, proveedor pp "
                        + "WHERE p.Id_proveedor = pp.ID_proveedor AND " + campoBuscar + " LIKE ?";
                
                PreparedStatement pst = connection.prepareStatement(query);
                pst.setString(1, '%' + palabraBuscada + '%');
                rs = pst.executeQuery();
            }
            else if(tabla.equals("cliente_pieza"))
            {
                String query = "SELECT cp.DNI_cliente, c.Nombre, c.Apellido, cp.ID_pieza, "
                            + "p.Nombre, cp.cantidad, cp.fecha_compra " 
                            + "FROM cliente c, pieza p, cliente_pieza cp " 
                            + "WHERE c.DNI = cp.DNI_cliente "
                            + "AND p.ID_pieza = cp.ID_pieza "
                            + "AND " + campoBuscar + " LIKE ?";
                
                System.out.println(query);
                PreparedStatement pst = connection.prepareStatement(query);
                pst.setString(1, '%' + palabraBuscada + '%');
                rs = pst.executeQuery();
            }
            else
            {
                String query = "SELECT * FROM " + tabla + " WHERE " 
                    + campoBuscar + " LIKE ?";
                
                PreparedStatement pst = connection.prepareStatement(query);
                pst.setString(1, '%' + palabraBuscada + '%');
                rs = pst.executeQuery();
            }
            
            return rs;
        } 
        catch (SQLException ex) 
        {
            System.out.println("ERROR:al consultar");
            ex.printStackTrace();
            return rs;
        }
    } 
    //--------------------------------------------------------------------------
    
    //--------------------------- Querys de clientes----------------------------
    public  void inserteClientes(String dni , String nombre, String apellido, 
            String correo, String direccion)
    {
        if(connection != null)
        {
            try 
            {
                Statement sta = connection.createStatement();
                sta.executeUpdate("INSERT INTO cliente (DNI, Nombre, Apellido, "
                        + "Email, Dirección) VALUES ('" 
                        + dni + "','" + nombre + "','" + apellido + "','"
                        + correo +"','" + direccion + "')");
                sta.close();
            } 
           catch (SQLException ex) 
            {
                System.out.println("ERROR:al insertar");
                ex.printStackTrace();                 
            }
        }
    }
    
    public void updateClientes(String dni , String nombre, String apellido, 
            String correo, String direccion)
    {
        if(connection != null)
        {
            try 
            {
                Statement sta = connection.createStatement();
                sta.executeUpdate("UPDATE cliente SET DNI = '"+ dni 
                        + "', Nombre = '" + nombre + "', Apellido = '" 
                        + apellido + "', Email = '" +  correo 
                        + "', Dirección = '" + direccion + "' WHERE DNI = '" 
                        + dni + "'");
                sta.close();
            } 
            catch (SQLException ex) 
            {
                System.out.println("ERROR:al actualizar");
                 ex.printStackTrace();            
            }
        }
    }
    //--------------------------------------------------------------------------
    
    //----------------------------Querys de pieza-------------------------------
    public  void insertaPieza(String nombrePieza , String fabricante, String descripcion, 
            String stock, String proveedor)
    {
        if(connection != null)
        {
            try 
            {
                Statement sta = connection.createStatement();
                String query = "INSERT INTO pieza(Nombre, Fabricante, Descripción, Stock, Id_proveedor) VALUES"
                        + "('" + nombrePieza + "', '" + fabricante + "', '" + descripcion + "','" + stock +"', " + proveedor + ")";
                System.out.println(query);
                sta.executeUpdate(query);
                sta.close();
            } 
           catch (SQLException ex) 
            {
                System.out.println("ERROR:al insertar");
                ex.printStackTrace();                 
            }
        }
    }
    
    public void updatePieza(String ID_pieza, String nombrePieza , String fabricante, String descripcion, 
            String stock, String proveedor)
    {
        if(connection != null)
        {
            try 
            {
                Statement sta = connection.createStatement();
                String query = "UPDATE pieza SET"
                        + " Nombre = '" + nombrePieza + "', Fabricante = '" 
                        + fabricante + "', Descripción = '" +  descripcion 
                        + "', Stock = '" + stock + "', Id_proveedor = " + proveedor + " WHERE ID_pieza = " 
                        + ID_pieza;
                
                System.out.println(query);
                
                sta.executeUpdate(query);              
                sta.close();
                
                System.out.println("He modificado la pieza");
            } 
            catch (SQLException ex) 
            {
                System.out.println("ERROR:al actualizar");
                 ex.printStackTrace();            
            }
        }
    }
    //--------------------------------------------------------------------------
    
    //------------------------Querys de proveedor-------------------------------
    public  void insertaProveedor(String nombre, String direccion)
    {
        if(connection != null)
        {
            try 
            {
                Statement sta = connection.createStatement();
                sta.executeUpdate("INSERT INTO proveedor (Nombre, Dirección) "
                        + "VALUES ('" + nombre + "', '" + direccion + "')");
                sta.close();
            } 
           catch (SQLException ex) 
            {
                System.out.println("ERROR:al insertar");
                ex.printStackTrace();                 
            }
        }
    }
    
    public void updatePropveedor(String ID_proveedor, String nombre, String direccion)
    {
        if(connection != null)
        {
            try 
            {
                Statement sta = connection.createStatement();
                sta.executeUpdate("UPDATE proveedor SET"
                        + " Nombre = '" + nombre + "', Dirección = '" 
                        + direccion + "' WHERE ID_proveedor = '" 
                        + ID_proveedor + "'");
                sta.close();
            } 
            catch (SQLException ex) 
            {
                System.out.println("ERROR:al actualizar");
                 ex.printStackTrace();            
            }
        }
    }
    //--------------------------------------------------------------------------
    
    //------------------------Querys de proveedor-------------------------------
    public  void insertaCompra(String dni, String pieza, String fecha_compra, String cantidad)
    {
        ResultSet rs = null;
        String numeroStock = "";
        int numeroStockActualizado;
        
        if(connection != null)
        {
            try 
            {
                connection.setAutoCommit(false);

                String query = "SELECT Stock FROM pieza WHERE ID_pieza = ?";
                PreparedStatement pst = connection.prepareStatement(query);
                pst.setString(1, pieza);
                rs = pst.executeQuery();
                while(rs.next())
                {
                    numeroStock = rs.getString("Stock");
                }
                
                numeroStockActualizado = Integer.parseInt(numeroStock) - Integer.parseInt(cantidad);
                pst.close();
                
                
                if(numeroStockActualizado >= 0)
                {
                  Statement sta = connection.createStatement();
                  sta.executeUpdate("INSERT INTO cliente_pieza (DNI_cliente, ID_pieza, fecha_compra, cantidad) "
                            + "VALUES ('" + dni +"','" + pieza + "','" + fecha_compra + "'," + cantidad + ")");
                  sta.close();
                  Statement sta2 = connection.createStatement();
                  sta2.executeUpdate("UPDATE pieza SET Stock = " + numeroStockActualizado +" WHERE ID_pieza = '" + pieza + "'");
                  sta2.close();
                  connection.setAutoCommit(true);
                }
            } 
            catch (SQLException ex) 
            {
                try
                {
                    if(connection!=null)
                    {
                        connection.rollback();
                    }
                }
                catch(SQLException se2)
                {
                    se2.printStackTrace();
                }
                System.out.println("ERROR:al insertar");
                ex.printStackTrace();
            }
        }
    }
    
    public void updateCompra(String DNI_cliente, String ID_Pieza, String fechaCompra, String cantidad)
    {
        if(connection != null)
        {
            try 
            {
                Statement sta = connection.createStatement();
                sta.executeUpdate("UPDATE cliente_pieza SET"
                        + " DNI_cliente = '" + DNI_cliente + "', ID_pieza = '" 
                        + ID_Pieza + "', fecha_compra = '" + fechaCompra + "', cantidad = '" + cantidad +"' WHERE ID_pieza = '" 
                        + ID_Pieza + "'");
                sta.close();
            } 
            catch (SQLException ex) 
            {
                System.out.println("ERROR:al actualizar");
                 ex.printStackTrace();            
            }
        }
    }
    //--------------------------------------------------------------------------
    
    
 
    public ResultSet todosLosProveedores(String tabla)
    {
        ResultSet rs = null;
        
        if(connection != null)
        {
            try
            {
                if(tabla.equals("pieza"))
                {    
                    Statement sta = connection.createStatement();
                    String query = "SELECT Nombre FROM " + tabla;
                    rs = sta.executeQuery(query);
                    return rs;
                }
                if(tabla.equals("proveedor"))
                {    
                    Statement sta = connection.createStatement();
                    String query = "SELECT Nombre FROM " + tabla;
                    rs = sta.executeQuery(query);
                    return rs;
                }
                else
                {
                    Statement sta = connection.createStatement();
                    String query = "SELECT DNI, Nombre, Apellido FROM " + tabla;
                    rs = sta.executeQuery(query);
                    return rs;
                }
            }
            catch (SQLException ex)
            {
                System.out.println("ERROR: al consultar todos los proveedores");
                ex.printStackTrace();   
            }
        }      
        return rs;
    }
    
    public  ResultSet obtenID_proveedor(String nombre)
    {
        ResultSet rs = null;
        
        if (connection != null)
        {
            try
            {
                String query = "SELECT ID_proveedor FROM proveedor WHERE Nombre = ?";
                PreparedStatement pst = connection.prepareStatement(query);
                pst.setString(1, nombre);
                rs = pst.executeQuery();
                return rs;
            }
            catch(SQLException ex)
            {
                System.out.println("ERROR: al consultar el ID");
                ex.printStackTrace();
            }
        }
        return rs;
    }
    
    public  ResultSet obtenID_pieza(String nombre)
    {
        ResultSet rs = null;
        
        if (connection != null)
        {
            try
            {
                String query = "SELECT ID_pieza FROM pieza WHERE Nombre = ?";
                PreparedStatement pst = connection.prepareStatement(query);
                pst.setString(1, nombre);
                rs = pst.executeQuery();
                return rs;
            }
            catch(SQLException ex)
            {
                System.out.println("ERROR: al consultar el ID_pieza");
                ex.printStackTrace();
            }
        }
        return rs;
    }
    
    public  ResultSet obtenDNI(String dni)
    {
        ResultSet rs = null;
        
        if (connection != null)
        {
            try
            {
                String query = "SELECT DNI FROM cliente WHERE DNI = ?";
                PreparedStatement pst = connection.prepareStatement(query);
                pst.setString(1, dni);
                rs = pst.executeQuery();
                return rs;
            }
            catch(SQLException ex)
            {
                System.out.println("ERROR: al consultar el ID");
                ex.printStackTrace();
            }
        }
        return rs;
    }
    
}
