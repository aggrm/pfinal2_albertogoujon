/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Alberto Goujon
 */
public class VentanaPrincipal extends javax.swing.JFrame {

    GestorConexion gesConex = new GestorConexion();
    
    public VentanaPrincipal() {
        initComponents();
        this.setLocationRelativeTo(null);
        setTitle("ComponetsUFV");
        
        
        //----------------------------------------------------------------------
        gesConex.conectameBBDD();                                               //conexion bbdd
        //----------------------------------------------------------------------
        
        //-----------------------------HagoTabla--------------------------------
        jTabbedPane1.setVisible(true);
        DefaultTableModel model = new DefaultTableModel();                          
        tablaClientes.setModel(model);                                          //Reiniciamos modelo de la tabla
        hazTabla("cliente", model);
        //----------------------------------------------------------------------
        
        //----------------Componentes que no quiero que se vean-----------------
        botonBorrarCliente.setVisible(false);
        botonBorrarPieza.setVisible(false);
        botonBorrarProveedor.setVisible(false);
        cajaIdPieza.setVisible(false);
        cajaIdPieza.setEditable(false);
        cajaIdProveedor.setVisible(false);
        cajaIdProveedor.setEditable(false);
        //----------------------------------------------------------------------
        hazJComboBox("proveedor");
        hazJComboBox("pieza");
        hazJComboBox("cliente");
        
    }

    
    //----------------------------Metodos dinamicos-----------------------------
    private void soloNumeros(java.awt.event.KeyEvent evt)
    {     
       char vchar = evt.getKeyChar();
       if(!(Character.isDigit(vchar)) || (vchar == KeyEvent.VK_BACK_SPACE))
       {
           evt.consume();
       } 
    }
    
    private void hazTabla(String consulta_Tabla, DefaultTableModel model)
    {
        ResultSet resultadoBuscado = gesConex.select_Tabla(consulta_Tabla);     //Le pasamos la select
        
        try 
        {
            
            int indiceColumna = 1;                                              //Índice para que me ponga los titulos de las columnas
            ResultSetMetaData rsMd = resultadoBuscado.getMetaData();            //Pasamos el resulset con formto de tabla
            int cantidadColumnas = rsMd.getColumnCount();                       //para saber cuantas columnas tengo
            for(int j = 0; j < cantidadColumnas; j++)                           //for para poner nombres a las columnas
            {
                model.addColumn(rsMd.getColumnName(indiceColumna++));       
            }
            while(resultadoBuscado.next())                                      //si no tenemos más resultados saldria del while
            {
                Object[] filas = new Object[cantidadColumnas];                  //Hacemos un array para guardar los datos 
                
                for(int i = 0; i < cantidadColumnas; i++)                       //for para ir poniendo todos los datos celda a celda
                {                                                               
                    filas[i] = resultadoBuscado.getObject(i + 1);
                }
                model.addRow(filas);                                            //para ir poniendo todos los datos fila a fila
                
        
            }
        } 
        catch (Exception e) 
        {
            System.out.println("no se ha podido hacer la tabla");
            e.printStackTrace();                                                
        }       
    }
    
    private void hazTablaFiltrada(ResultSet resultado, JTable tabla)
    {
        try 
        {
            int indiceColumna = 1;                                              //indice para que me ponga los titulos de las columnas
            DefaultTableModel model = new DefaultTableModel();                        
            tabla.setModel(model);                                      //Reiniciamos modelo de la tabla
            ResultSetMetaData rsMd = resultado.getMetaData();                   //Pasamos el resulset con formto de tabla
            int cantidadColumnas = rsMd.getColumnCount();                       //para saber cuantas columnas tengo
            for(int j = 0; j < cantidadColumnas; j++)                           //for para poner nobres a las columnas
            {
                model.addColumn(rsMd.getColumnName(indiceColumna++));       
            }
            while(resultado.next())                                      //si no tenemos más resultados saldria del while
            {
                Object[] filas = new Object[cantidadColumnas];                  //Hacemos un array para guardar los datos 
                
                for(int i = 0; i < cantidadColumnas; i++)                       //for para ir poniendo todos los datos celda a celda
                {                                                               
                    filas[i] = resultado.getObject(i + 1);
                }
                model.addRow(filas);                                            //para ir poniendo todos los datos fila a fila
            }
        } 
        catch (Exception e) 
        {
            System.out.println("no se ha podido hacer la tabla");
            e.printStackTrace();                                                
        }       
    }
    
    private void hazJComboBox(String tabla)
    {
        ResultSet rs = gesConex.todosLosProveedores(tabla);
        try {
            if(tabla.equals("proveedor"))
            {
                while(rs.next()) 
                {
                    cajaProveedorPieza.addItem(rs.getString("Nombre"));
                }
            }
            if(tabla.equals("pieza"))
            {
                while(rs.next()) 
                {
                    cajaIDPiezaCompra.addItem(rs.getString("Nombre"));
                }
            }
            if(tabla.equals("cliente"))
            {
                while(rs.next()) 
                {
                    String resultado = rs.getString("DNI") + "_"  + rs.getString("Nombre") + " " + rs.getString("Apellido");
                    cajaDNICompra.addItem(resultado);
                }
            }
            
        } catch (Exception ex) 
        {
            ex.printStackTrace();
        }
        
    }
    //--------------------------------------------------------------------------
    
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jToggleButton1 = new javax.swing.JToggleButton();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        panelClientes = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaClientes = new javax.swing.JTable();
        botonInsertarCliente = new javax.swing.JButton();
        dni = new javax.swing.JLabel();
        nombre = new javax.swing.JLabel();
        apellido = new javax.swing.JLabel();
        correo = new javax.swing.JLabel();
        direccion = new javax.swing.JLabel();
        cajaDNI = new javax.swing.JTextField();
        cajaNombre = new javax.swing.JTextField();
        cajaApellido = new javax.swing.JTextField();
        cajaCorreo = new javax.swing.JTextField();
        cajaDireccion = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        comboBoxClientes = new javax.swing.JComboBox<>();
        cajaBuquedaClientes = new javax.swing.JTextField();
        botonBuscarCliente = new javax.swing.JButton();
        botonBorrarCliente = new javax.swing.JButton();
        panelPiezas = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tablaPiezas = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        comboBoxPiezas = new javax.swing.JComboBox<>();
        cajaBuquedaPieza = new javax.swing.JTextField();
        botonBuscarPieza = new javax.swing.JButton();
        nombrePieza = new javax.swing.JLabel();
        fabricante = new javax.swing.JLabel();
        cajaFabricante = new javax.swing.JTextField();
        cajaNombrePieza = new javax.swing.JTextField();
        stock = new javax.swing.JLabel();
        descricion = new javax.swing.JLabel();
        proveedor = new javax.swing.JLabel();
        cajaDescripcion = new javax.swing.JTextField();
        cajaStock = new javax.swing.JTextField();
        botonInsertarPieza = new javax.swing.JButton();
        botonBorrarPieza = new javax.swing.JButton();
        cajaIdPieza = new javax.swing.JTextField();
        cajaProveedorPieza = new javax.swing.JComboBox<>();
        panelProveedores = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        comboBoxProveedor = new javax.swing.JComboBox<>();
        cajaBuquedaProveedor = new javax.swing.JTextField();
        botonBuscarproveedor = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        tablaProveedores = new javax.swing.JTable();
        nombreProveedor = new javax.swing.JLabel();
        cajaNombreProveedor = new javax.swing.JTextField();
        direcionProveedor = new javax.swing.JLabel();
        cajaDireccionProveedor = new javax.swing.JTextField();
        botonInsertarProveedor = new javax.swing.JButton();
        botonBorrarProveedor = new javax.swing.JButton();
        cajaIdProveedor = new javax.swing.JTextField();
        panelCompras = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        comboBoxCompra = new javax.swing.JComboBox<>();
        cajaBuquedaCompra = new javax.swing.JTextField();
        botonBuscarCompra = new javax.swing.JButton();
        jScrollPane4 = new javax.swing.JScrollPane();
        tablaCompras = new javax.swing.JTable();
        dniCcompra = new javax.swing.JLabel();
        idPiezaCompra = new javax.swing.JLabel();
        fechaCompra = new javax.swing.JLabel();
        cajaFechaCompra = new javax.swing.JTextField();
        cantidad = new javax.swing.JLabel();
        cajaCantidadCCompra = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        cajaFechaCompra1 = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        cajaFechaCompra2 = new javax.swing.JTextField();
        botonInsertarCompra = new javax.swing.JButton();
        fechaCompra1 = new javax.swing.JLabel();
        cajaDNICompra = new javax.swing.JComboBox<>();
        cajaIDPiezaCompra = new javax.swing.JComboBox<>();
        botonConexion = new javax.swing.JToggleButton();
        panelConexion = new javax.swing.JPanel();

        jToggleButton1.setText("jToggleButton1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jTabbedPane1.setOpaque(true);
        jTabbedPane1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jTabbedPane1MousePressed(evt);
            }
        });

        tablaClientes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tablaClientesMousePressed(evt);
            }
        });
        jScrollPane1.setViewportView(tablaClientes);

        botonInsertarCliente.setText("Insertar cliente");
        botonInsertarCliente.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                botonInsertarClienteMousePressed(evt);
            }
        });

        dni.setText("DNI * :");

        nombre.setText("Nombre:");

        apellido.setText("Apellido:");

        correo.setText("Correo:");

        direccion.setText("Dirección:");

        jLabel1.setText("Buscar por:");

        comboBoxClientes.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "DNI", "Nombre", "Apellido", "Correo", "Dirección" }));

        botonBuscarCliente.setText("Buscar");
        botonBuscarCliente.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                botonBuscarClienteMousePressed(evt);
            }
        });

        botonBorrarCliente.setText("Borrar cliente");
        botonBorrarCliente.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                botonBorrarClienteMousePressed(evt);
            }
        });

        javax.swing.GroupLayout panelClientesLayout = new javax.swing.GroupLayout(panelClientes);
        panelClientes.setLayout(panelClientesLayout);
        panelClientesLayout.setHorizontalGroup(
            panelClientesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelClientesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelClientesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelClientesLayout.createSequentialGroup()
                        .addComponent(nombre)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cajaNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(97, 97, 97)
                        .addGroup(panelClientesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelClientesLayout.createSequentialGroup()
                                .addComponent(correo, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cajaCorreo, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(panelClientesLayout.createSequentialGroup()
                                .addComponent(apellido)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cajaApellido, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 44, Short.MAX_VALUE)
                                .addComponent(direccion)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cajaDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())))
                    .addGroup(panelClientesLayout.createSequentialGroup()
                        .addComponent(dni, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cajaDNI, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelClientesLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(botonBorrarCliente)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(botonInsertarCliente)
                .addContainerGap())
            .addComponent(jScrollPane1)
            .addGroup(panelClientesLayout.createSequentialGroup()
                .addGap(116, 116, 116)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(comboBoxClientes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cajaBuquedaClientes, javax.swing.GroupLayout.PREFERRED_SIZE, 404, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(botonBuscarCliente)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        panelClientesLayout.setVerticalGroup(
            panelClientesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelClientesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelClientesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(comboBoxClientes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cajaBuquedaClientes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(botonBuscarCliente))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(panelClientesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(dni)
                    .addComponent(cajaDNI, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(apellido)
                    .addComponent(cajaApellido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(direccion)
                    .addComponent(cajaDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panelClientesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cajaNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(nombre)
                    .addComponent(correo)
                    .addComponent(cajaCorreo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(70, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelClientesLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panelClientesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(botonBorrarCliente)
                    .addComponent(botonInsertarCliente))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Clientes", panelClientes);

        tablaPiezas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tablaPiezas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tablaPiezasMousePressed(evt);
            }
        });
        jScrollPane2.setViewportView(tablaPiezas);

        jLabel2.setText("Buscar por:");

        comboBoxPiezas.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "ID_pieza", "Nombre", "Fabricante", "Descripción", "Stock", "Id_proveedor" }));

        botonBuscarPieza.setText("Buscar");
        botonBuscarPieza.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                botonBuscarPiezaMousePressed(evt);
            }
        });

        nombrePieza.setText("Nombre * :");

        fabricante.setText("Fabricante:");

        stock.setText("Stock:");

        descricion.setText("Descripción:");

        proveedor.setText("Proveedor:");

        cajaStock.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                cajaStockKeyTyped(evt);
            }
        });

        botonInsertarPieza.setText("Insertar pieza");
        botonInsertarPieza.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                botonInsertarPiezaMousePressed(evt);
            }
        });

        botonBorrarPieza.setText("Borrar pieza");
        botonBorrarPieza.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                botonBorrarPiezaMousePressed(evt);
            }
        });

        javax.swing.GroupLayout panelPiezasLayout = new javax.swing.GroupLayout(panelPiezas);
        panelPiezas.setLayout(panelPiezasLayout);
        panelPiezasLayout.setHorizontalGroup(
            panelPiezasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPiezasLayout.createSequentialGroup()
                .addGap(117, 117, 117)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(comboBoxPiezas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cajaBuquedaPieza, javax.swing.GroupLayout.PREFERRED_SIZE, 404, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(botonBuscarPieza)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelPiezasLayout.createSequentialGroup()
                .addGroup(panelPiezasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelPiezasLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(panelPiezasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(botonBorrarPieza)
                            .addGroup(panelPiezasLayout.createSequentialGroup()
                                .addGroup(panelPiezasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(panelPiezasLayout.createSequentialGroup()
                                        .addComponent(nombrePieza, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(cajaNombrePieza, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(panelPiezasLayout.createSequentialGroup()
                                        .addGroup(panelPiezasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addGroup(panelPiezasLayout.createSequentialGroup()
                                                .addGap(346, 346, 346)
                                                .addComponent(descricion))
                                            .addGroup(panelPiezasLayout.createSequentialGroup()
                                                .addComponent(fabricante)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(cajaFabricante, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(stock, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(panelPiezasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(cajaDescripcion, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(cajaStock, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addGap(18, 18, 18)
                                .addComponent(proveedor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelPiezasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(panelPiezasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(cajaIdPieza, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(cajaProveedorPieza, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(botonInsertarPieza))))
                .addGap(82, 82, 82))
        );
        panelPiezasLayout.setVerticalGroup(
            panelPiezasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelPiezasLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelPiezasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(comboBoxPiezas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cajaBuquedaPieza, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(botonBuscarPieza))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(panelPiezasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nombrePieza)
                    .addComponent(cajaNombrePieza, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(descricion)
                    .addComponent(proveedor)
                    .addComponent(cajaDescripcion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cajaProveedorPieza, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panelPiezasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cajaFabricante, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fabricante)
                    .addComponent(stock)
                    .addComponent(cajaStock, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cajaIdPieza, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 30, Short.MAX_VALUE)
                .addGroup(panelPiezasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(botonInsertarPieza)
                    .addComponent(botonBorrarPieza))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Piezas", panelPiezas);

        jLabel3.setText("Buscar por:");

        comboBoxProveedor.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "ID_proveedor", "Nombre", "Dirección" }));

        botonBuscarproveedor.setText("Buscar");
        botonBuscarproveedor.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                botonBuscarproveedorMousePressed(evt);
            }
        });

        tablaProveedores.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {}
            },
            new String [] {

            }
        ));
        tablaProveedores.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tablaProveedoresMousePressed(evt);
            }
        });
        jScrollPane3.setViewportView(tablaProveedores);

        nombreProveedor.setText("Nombre * : ");

        direcionProveedor.setText("Dirección:");

        botonInsertarProveedor.setText("Insertar proveedor");
        botonInsertarProveedor.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                botonInsertarProveedorMousePressed(evt);
            }
        });

        botonBorrarProveedor.setText("Borrar proveedor");
        botonBorrarProveedor.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                botonBorrarProveedorMousePressed(evt);
            }
        });

        javax.swing.GroupLayout panelProveedoresLayout = new javax.swing.GroupLayout(panelProveedores);
        panelProveedores.setLayout(panelProveedoresLayout);
        panelProveedoresLayout.setHorizontalGroup(
            panelProveedoresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(panelProveedoresLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelProveedoresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelProveedoresLayout.createSequentialGroup()
                        .addComponent(botonBorrarProveedor)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(botonInsertarProveedor))
                    .addGroup(panelProveedoresLayout.createSequentialGroup()
                        .addGroup(panelProveedoresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(nombreProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(direcionProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelProveedoresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelProveedoresLayout.createSequentialGroup()
                                .addComponent(cajaDireccionProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(panelProveedoresLayout.createSequentialGroup()
                                .addComponent(cajaNombreProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(cajaIdProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap())
            .addGroup(panelProveedoresLayout.createSequentialGroup()
                .addGap(120, 120, 120)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(comboBoxProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cajaBuquedaProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, 404, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(botonBuscarproveedor)
                .addContainerGap(118, Short.MAX_VALUE))
        );
        panelProveedoresLayout.setVerticalGroup(
            panelProveedoresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelProveedoresLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelProveedoresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(comboBoxProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cajaBuquedaProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(botonBuscarproveedor))
                .addGap(4, 4, 4)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelProveedoresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nombreProveedor)
                    .addComponent(cajaNombreProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cajaIdProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panelProveedoresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(direcionProveedor)
                    .addComponent(cajaDireccionProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 40, Short.MAX_VALUE)
                .addGroup(panelProveedoresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(botonBorrarProveedor)
                    .addComponent(botonInsertarProveedor))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Proveedores", panelProveedores);

        jLabel4.setText("Buscar por:");

        comboBoxCompra.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "DNI_cliente", "ID_pieza", "fecha_compra" }));

        botonBuscarCompra.setText("Buscar");
        botonBuscarCompra.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                botonBuscarCompraMousePressed(evt);
            }
        });

        tablaCompras.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane4.setViewportView(tablaCompras);

        dniCcompra.setText("DNI_cliente:");

        idPiezaCompra.setText("Pieza:");

        fechaCompra.setText("fecha_compra:");

        cajaFechaCompra.setText("año");
        cajaFechaCompra.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                cajaFechaCompraMousePressed(evt);
            }
        });
        cajaFechaCompra.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                cajaFechaCompraKeyTyped(evt);
            }
        });

        cantidad.setText("Cantida:");

        jLabel5.setText("-");

        cajaFechaCompra1.setText("mes");
        cajaFechaCompra1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                cajaFechaCompra1MousePressed(evt);
            }
        });
        cajaFechaCompra1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                cajaFechaCompra1KeyTyped(evt);
            }
        });

        jLabel6.setText("-");

        cajaFechaCompra2.setText("día");
        cajaFechaCompra2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                cajaFechaCompra2MousePressed(evt);
            }
        });
        cajaFechaCompra2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                cajaFechaCompra2KeyTyped(evt);
            }
        });

        botonInsertarCompra.setText("Agregar Compra");
        botonInsertarCompra.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                botonInsertarCompraMousePressed(evt);
            }
        });

        fechaCompra1.setText("yyyy - mm - dd");

        javax.swing.GroupLayout panelComprasLayout = new javax.swing.GroupLayout(panelCompras);
        panelCompras.setLayout(panelComprasLayout);
        panelComprasLayout.setHorizontalGroup(
            panelComprasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelComprasLayout.createSequentialGroup()
                .addGroup(panelComprasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelComprasLayout.createSequentialGroup()
                        .addGap(121, 121, 121)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(comboBoxCompra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cajaBuquedaCompra, javax.swing.GroupLayout.PREFERRED_SIZE, 404, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(botonBuscarCompra)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(panelComprasLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 903, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelComprasLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(botonInsertarCompra))
                    .addGroup(panelComprasLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(panelComprasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(idPiezaCompra, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(dniCcompra, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelComprasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(cajaDNICompra, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cajaIDPiezaCompra, 0, 197, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(panelComprasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelComprasLayout.createSequentialGroup()
                                .addComponent(fechaCompra)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cajaFechaCompra, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cajaFechaCompra1, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cajaFechaCompra2, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(fechaCompra1))
                            .addGroup(panelComprasLayout.createSequentialGroup()
                                .addComponent(cantidad, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cajaCantidadCCompra, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap())
        );
        panelComprasLayout.setVerticalGroup(
            panelComprasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelComprasLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelComprasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cajaBuquedaCompra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(botonBuscarCompra)
                    .addComponent(comboBoxCompra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(panelComprasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(dniCcompra)
                    .addComponent(fechaCompra)
                    .addComponent(cajaFechaCompra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(cajaFechaCompra1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(cajaFechaCompra2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fechaCompra1)
                    .addComponent(cajaDNICompra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panelComprasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(idPiezaCompra)
                    .addComponent(cantidad)
                    .addComponent(cajaCantidadCCompra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cajaIDPiezaCompra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 28, Short.MAX_VALUE)
                .addComponent(botonInsertarCompra)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Compras", panelCompras);

        botonConexion.setSelected(true);
        botonConexion.setText("Conectar a la base de Datos");
        botonConexion.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                botonConexionMouseClicked(evt);
            }
        });

        panelConexion.setBackground(new java.awt.Color(51, 255, 51));

        javax.swing.GroupLayout panelConexionLayout = new javax.swing.GroupLayout(panelConexion);
        panelConexion.setLayout(panelConexionLayout);
        panelConexionLayout.setHorizontalGroup(
            panelConexionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 41, Short.MAX_VALUE)
        );
        panelConexionLayout.setVerticalGroup(
            panelConexionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 48, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 917, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(botonConexion)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelConexion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jTabbedPane1)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(botonConexion, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(panelConexion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


//--------------------------------Panel y conexion bbdd-------------------------
    private void botonConexionMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_botonConexionMouseClicked
        if(botonConexion.isSelected())
        {
            gesConex.conectameBBDD();
            botonConexion.setText("Desconectar de la Base de Datos");
            panelConexion.setBackground(Color.GREEN);
            
            jTabbedPane1.setVisible(true);
            DefaultTableModel model = new DefaultTableModel();                  
            tablaClientes.setModel(model);                                      //Reiniciamos modelo de la tabla
            hazTabla("cliente", model);
        }
        else if(!botonConexion.isSelected())
        {
            gesConex.desconectaBBDD();
            botonConexion.setText("Conectar a la Base de Datos");
            panelConexion.setBackground(Color.RED);  
            jTabbedPane1.setVisible(false);
        }
    }//GEN-LAST:event_botonConexionMouseClicked

    private void jTabbedPane1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTabbedPane1MousePressed
        if(jTabbedPane1.getSelectedIndex() == 0)
        {
            DefaultTableModel model = new DefaultTableModel();                  
            tablaClientes.setModel(model);                                      //Reiniciamos modelo de la tabla
            hazTabla("cliente", model);
        }
        if(jTabbedPane1.getSelectedIndex() == 1)
        {
            DefaultTableModel model = new DefaultTableModel();                  
            tablaPiezas.setModel(model);                                        //Reiniciamos modelo de la tabla
            hazTabla("pieza", model);
        }
        if(jTabbedPane1.getSelectedIndex() == 2)
        {
            DefaultTableModel model = new DefaultTableModel();                  
            tablaProveedores.setModel(model);                                   //Reiniciamos modelo de la tabla
            hazTabla("proveedor", model);
        }
        if(jTabbedPane1.getSelectedIndex() == 3)
        {
            DefaultTableModel model = new DefaultTableModel();                  
            tablaCompras.setModel(model);                                       //Reiniciamos modelo de la tabla
            hazTabla("cliente_pieza", model);
        }
        
    }//GEN-LAST:event_jTabbedPane1MousePressed
//------------------------------------------------------------------------------
    
    
//--------------------------------------Clientes--------------------------------
    private void botonInsertarClienteMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_botonInsertarClienteMousePressed
        if(jTabbedPane1.getSelectedIndex() == 0)
        {
            if(!cajaDNI.getText().equals(""))
            {
               if(botonInsertarCliente.getText().equals("Insertar cliente"))
                {
                    gesConex.inserteClientes(cajaDNI.getText(), cajaNombre.getText(), cajaApellido.getText(), cajaCorreo.getText(), cajaDireccion.getText());
                    cajaDNI.setText("");
                    cajaNombre.setText("");
                    cajaApellido.setText("");
                    cajaCorreo.setText("");
                    cajaDireccion.setText("");                    
                    
                    DefaultTableModel model = new DefaultTableModel();                  
                    tablaClientes.setModel(model);                                  //Reiniciamos modelo de la tabla
                    hazTabla("cliente", model);
                } 
               else if(botonInsertarCliente.getText().equals("Actualizar cliente"))
                {
                    gesConex.updateClientes(cajaDNI.getText(), cajaNombre.getText(), cajaApellido.getText(), cajaCorreo.getText(), cajaDireccion.getText());
                    cajaDNI.setText("");
                    cajaDNI.setEditable(true);
                    cajaNombre.setText("");
                    cajaApellido.setText("");
                    cajaCorreo.setText("");
                    cajaDireccion.setText("");
                    botonBorrarCliente.setVisible(false);
                    botonInsertarCliente.setText("Insertar cliente");

                    DefaultTableModel model = new DefaultTableModel();                  
                    tablaClientes.setModel(model);                                  //Reiniciamos modelo de la tabla
                    hazTabla("cliente", model);
                    
                }
               hazJComboBox("cliente");
            }
            else
            {
                String message = "Al menos ccomplete el campo del DNI";
                JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
                JOptionPane.ERROR_MESSAGE);
            }
            
        }   
    }//GEN-LAST:event_botonInsertarClienteMousePressed

    private void tablaClientesMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaClientesMousePressed
        DefaultTableModel model = (DefaultTableModel)tablaClientes.getModel();
        int selectRowIndex = tablaClientes.getSelectedRow();
        
        cajaDNI.setText(model.getValueAt(selectRowIndex, 0).toString());
        cajaDNI.setEditable(false);
        cajaNombre.setText(model.getValueAt(selectRowIndex, 1).toString());
        cajaApellido.setText(model.getValueAt(selectRowIndex, 2).toString());
        cajaCorreo.setText(model.getValueAt(selectRowIndex, 3).toString());
        cajaDireccion.setText(model.getValueAt(selectRowIndex, 4).toString());
        botonBorrarCliente.setVisible(true);
        botonInsertarCliente.setText("Actualizar cliente");
    }//GEN-LAST:event_tablaClientesMousePressed

    private void botonBorrarClienteMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_botonBorrarClienteMousePressed
        gesConex.delete("cliente", "DNI", cajaDNI.getText());         
       
        cajaDNI.setText("");
        cajaDNI.setEditable(true);
        cajaNombre.setText("");
        cajaApellido.setText("");
        cajaCorreo.setText("");
        cajaDireccion.setText("");
        botonInsertarCliente.setText("Insertar cliente");
        botonBorrarCliente.setVisible(false);
        
        DefaultTableModel model = new DefaultTableModel();                        
        tablaClientes.setModel(model);                                          //Reiniciamos modelo de la tabla
        hazTabla("cliente", model);
        
        
        DefaultComboBoxModel aModel = new DefaultComboBoxModel();
        cajaDNICompra.setModel(aModel);                                         //Reiniciamos modelo del comboBox    
        hazJComboBox("cliente");
    }//GEN-LAST:event_botonBorrarClienteMousePressed

    private void botonBuscarClienteMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_botonBuscarClienteMousePressed
        String campoSeleccionado = comboBoxClientes.getSelectedItem().toString();
        String palabraBuscada = cajaBuquedaClientes.getText();
        ResultSet resultadoFiltrado = gesConex.buscador("cliente", campoSeleccionado, palabraBuscada);
        hazTablaFiltrada(resultadoFiltrado, tablaClientes);
    }//GEN-LAST:event_botonBuscarClienteMousePressed
//------------------------------------------------------------------------------
    
    
    
//-----------------------------------Piezas-------------------------------------
    private void botonBorrarPiezaMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_botonBorrarPiezaMousePressed
        gesConex.delete("pieza", "ID_pieza", cajaIdPieza.getText());         
       
        cajaIdPieza.setText("");
        cajaNombrePieza.setText("");
        cajaDescripcion.setText("");
        cajaFabricante.setText("");
        cajaStock.setText("");
        botonInsertarPieza.setText("Insertar pieza");
        botonBorrarPieza.setVisible(false);
        
        
        DefaultTableModel model = new DefaultTableModel();                        
        tablaPiezas.setModel(model);                                          //Reiniciamos modelo de la tabla
        hazTabla("pieza", model);
        
        DefaultComboBoxModel aModel = new DefaultComboBoxModel();
        cajaDNICompra.setModel(aModel);                                         //Reiniciamos modelo del comboBox    
        hazJComboBox("pieza");
    }//GEN-LAST:event_botonBorrarPiezaMousePressed

    
    private void tablaPiezasMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaPiezasMousePressed
        
        DefaultTableModel model = (DefaultTableModel)tablaPiezas.getModel();
        int selectRowIndex = tablaPiezas.getSelectedRow();    
        
        String nombreFab = model.getValueAt(selectRowIndex, 6).toString();
        
        cajaIdPieza.setText(model.getValueAt(selectRowIndex, 0).toString());
        cajaNombrePieza.setText(model.getValueAt(selectRowIndex, 1).toString());
        cajaFabricante.setText(model.getValueAt(selectRowIndex, 2).toString());
        cajaDescripcion.setText(model.getValueAt(selectRowIndex, 3).toString());
        cajaStock.setText(model.getValueAt(selectRowIndex, 4).toString());
        cajaProveedorPieza.setSelectedItem(nombreFab);
        botonBorrarPieza.setVisible(true);
        botonInsertarPieza.setText("Actualizar pieza");
    }//GEN-LAST:event_tablaPiezasMousePressed

    private void botonInsertarPiezaMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_botonInsertarPiezaMousePressed
        if(jTabbedPane1.getSelectedIndex() == 1)
        {
            if(!cajaNombrePieza.getText().equals(""))
            {
                if(botonInsertarPieza.getText().equals("Insertar pieza"))
                {
                    try 
                    {
                        //-----------------------Caja proveedor pieza-----------
                        String nombreProv = cajaProveedorPieza.getSelectedItem().toString();
                        ResultSet rsIdProveedor = gesConex.obtenID_proveedor(nombreProv);
                        String ID_proveedor = "";
                        while (rsIdProveedor.next()) 
                        {
                            ID_proveedor = rsIdProveedor.getString("ID_proveedor");
                            System.out.println(ID_proveedor);
                        }
                        //------------------------------------------------------
                        
                        
                        gesConex.insertaPieza(cajaNombrePieza.getText(), cajaFabricante.getText(), cajaDescripcion.getText(), cajaStock.getText(), ID_proveedor);
                        cajaNombrePieza.setText("");
                        cajaFabricante.setText("");
                        cajaDescripcion.setText("");
                        cajaStock.setText("");
                        cajaIdProveedor.setText("");
                        
                        DefaultTableModel model = new DefaultTableModel();
                        tablaPiezas.setModel(model);                                //Reiniciamos modelo de la tabla
                        hazTabla("pieza", model);
                    } 
                    catch (SQLException ex) 
                    {
                        ex.printStackTrace();
                    }
                }
                else if(botonInsertarPieza.getText().equals("Actualizar pieza"))
                {
                    try 
                    {
                        String nombreProv = cajaProveedorPieza.
                                getSelectedItem().toString();                       //Pillo el nombre del fabricante

                        ResultSet rsIdProveedor = gesConex.
                                obtenID_proveedor(nombreProv);                      //obtengo Id del proveedor
                        String ID_proveedor = "";
                        while (rsIdProveedor.next())
                        {
                            ID_proveedor = rsIdProveedor.getString("ID_proveedor"); //meto el ID del proveedor en el String 
                        }

                        gesConex.updatePieza(cajaIdPieza.getText(), cajaNombrePieza.getText(), cajaFabricante.getText(), cajaDescripcion.getText(), cajaStock.getText(), ID_proveedor);
                        cajaNombrePieza.setText("");
                        cajaFabricante.setText("");
                        cajaDescripcion.setText("");
                        cajaStock.setText("");
                        cajaIdProveedor.setText("");
                        botonBorrarPieza.setVisible(false);
                        botonInsertarPieza.setText("Insertar pieza");

                        DefaultTableModel model = new DefaultTableModel();
                        tablaPiezas.setModel(model);                                  //Reiniciamos modelo de la tabla
                        hazTablaFiltrada(gesConex.select_Tabla("pieza"), tablaPiezas);
                    } 
                    catch (SQLException ex) 
                    {
                        ex.printStackTrace();
                    }
                }
                hazJComboBox("pieza");
            }
            else
            {
                String message = "Al menos complete el campo del Nombre";
                JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
                JOptionPane.ERROR_MESSAGE);
            }
        }   
    }//GEN-LAST:event_botonInsertarPiezaMousePressed
//------------------------------------------------------------------------------

    
//----------------------------------Proveedores---------------------------------    
    private void botonInsertarProveedorMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_botonInsertarProveedorMousePressed
        if(jTabbedPane1.getSelectedIndex() == 2)
        {
            if(!cajaNombreProveedor.getText().equals(""))
            {
                if(botonInsertarProveedor.getText().equals("Insertar proveedor"))
                {
                    gesConex.insertaProveedor(cajaNombreProveedor.getText(), cajaDireccionProveedor.getText());
                    DefaultTableModel model = new DefaultTableModel();                  

                    tablaProveedores.setModel(model);                                  //Reiniciamos modelo de la tabla
                    hazTabla("proveedor", model);

                    //-------------Reiniciamos cajas a vacio------------------------
                    cajaIdProveedor.setText("");
                    cajaNombreProveedor.setText("");
                    cajaDireccionProveedor.setText("");
                    //--------------------------------------------------------------
                    hazJComboBox("proveedor");
                }
                else if(botonInsertarProveedor.getText().equals("Actualizar proveedor"))
                {
                    gesConex.updatePropveedor(cajaIdProveedor.getText(), cajaNombreProveedor.getText(), cajaDireccionProveedor.getText());
                    cajaNombreProveedor.setText("");
                    //cajaFabricante.setEditable(true);
                    cajaIdProveedor.setText("");
                    cajaNombreProveedor.setText("");
                    cajaDireccionProveedor.setText("");
                    botonInsertarPieza.setText("Insertar proveedor");

                    DefaultTableModel model = new DefaultTableModel();                  
                    tablaProveedores.setModel(model);                                  //Reiniciamos modelo de la tabla
                    hazTabla("proveedor", model);
                }
                hazJComboBox("proveedor");
            }
            else
            {
                String message = "Al menos complete el campo del Nombre";
                JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
                JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_botonInsertarProveedorMousePressed

    private void tablaProveedoresMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaProveedoresMousePressed
        DefaultTableModel model = (DefaultTableModel)tablaProveedores.getModel();
        int selectRowIndex = tablaProveedores.getSelectedRow();
        
        
        cajaIdProveedor.setText(model.getValueAt(selectRowIndex, 0).toString());
        cajaNombreProveedor.setText(model.getValueAt(selectRowIndex, 1).toString());
        cajaDireccionProveedor.setText(model.getValueAt(selectRowIndex, 2).toString());
        botonBorrarProveedor.setVisible(true);
        botonInsertarProveedor.setText("Actualizar proveedor");
    }//GEN-LAST:event_tablaProveedoresMousePressed

    private void botonBorrarProveedorMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_botonBorrarProveedorMousePressed
        gesConex.delete("proveedor", "ID_proveedor", cajaIdProveedor.getText());
        
        cajaNombreProveedor.setText("");
        cajaIdProveedor.setText("");
        cajaNombreProveedor.setText("");
        cajaDireccionProveedor.setText("");
        botonInsertarPieza.setText("Insertar proveedor");
        
        DefaultTableModel model = new DefaultTableModel();                  
        tablaProveedores.setModel(model);                                  //Reiniciamos modelo de la tabla
        hazTabla("proveedor", model);
        
        DefaultComboBoxModel aModel = new DefaultComboBoxModel();
        cajaDNICompra.setModel(aModel);                                         //Reiniciamos modelo del comboBox    
        hazJComboBox("proveedor");
    }//GEN-LAST:event_botonBorrarProveedorMousePressed
    //--------------------------------------------------------------------------
    
    //-----------------------------Limitación campos----------------------------
    private void cajaStockKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cajaStockKeyTyped
        soloNumeros(evt);
    }//GEN-LAST:event_cajaStockKeyTyped

    private void cajaFechaCompraKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cajaFechaCompraKeyTyped
        soloNumeros(evt);
    }//GEN-LAST:event_cajaFechaCompraKeyTyped

    private void cajaFechaCompra1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cajaFechaCompra1KeyTyped
        soloNumeros(evt);
    }//GEN-LAST:event_cajaFechaCompra1KeyTyped

    private void cajaFechaCompra2KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cajaFechaCompra2KeyTyped
        soloNumeros(evt);
    }//GEN-LAST:event_cajaFechaCompra2KeyTyped
    //--------------------------------------------------------------------------
    
    //-----------------------------Buscadores-----------------------------------
    private void botonBuscarPiezaMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_botonBuscarPiezaMousePressed
        String campoSeleccionado = comboBoxPiezas.getSelectedItem().toString();
        String palabraBuscada = cajaBuquedaPieza.getText();
        ResultSet resultadoFiltrado = gesConex.buscador("pieza", campoSeleccionado, palabraBuscada);
        hazTablaFiltrada(resultadoFiltrado, tablaPiezas);
    }//GEN-LAST:event_botonBuscarPiezaMousePressed

    private void botonBuscarproveedorMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_botonBuscarproveedorMousePressed
        String campoSeleccionado = comboBoxProveedor.getSelectedItem().toString();
        String palabraBuscada = cajaBuquedaProveedor.getText();
        ResultSet resultadoFiltrado = gesConex.buscador("proveedor", campoSeleccionado, palabraBuscada);
        hazTablaFiltrada(resultadoFiltrado, tablaProveedores);
    }//GEN-LAST:event_botonBuscarproveedorMousePressed

    private void botonBuscarCompraMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_botonBuscarCompraMousePressed
        String campoSeleccionado = comboBoxCompra.getSelectedItem().toString();
        String palabraBuscada = cajaBuquedaCompra.getText();
        ResultSet resultadoFiltrado = gesConex.buscador("cliente_pieza", campoSeleccionado, palabraBuscada);
        hazTablaFiltrada(resultadoFiltrado, tablaProveedores);
    }//GEN-LAST:event_botonBuscarCompraMousePressed
    //--------------------------------------------------------------------------
    
    //------------Borrar lo que hay puesto en los campos de la fecha------------
    private void cajaFechaCompraMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cajaFechaCompraMousePressed
        cajaFechaCompra.setText("");
    }//GEN-LAST:event_cajaFechaCompraMousePressed

    private void cajaFechaCompra1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cajaFechaCompra1MousePressed
        cajaFechaCompra1.setText("");
    }//GEN-LAST:event_cajaFechaCompra1MousePressed

    private void cajaFechaCompra2MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cajaFechaCompra2MousePressed
        cajaFechaCompra2.setText("");
    }//GEN-LAST:event_cajaFechaCompra2MousePressed
    //--------------------------------------------------------------------------
    
    //----------------------------Compras de piezas-----------------------------
    private void botonInsertarCompraMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_botonInsertarCompraMousePressed
        if(jTabbedPane1.getSelectedIndex() == 3)
        {
            if(Integer.parseInt(cajaCantidadCCompra.getText()) > 0 
                    || Integer.parseInt(cajaFechaCompra.getText()) > 1960 
                    || Integer.parseInt(cajaFechaCompra1.getText()) > 00
                    || Integer.parseInt(cajaFechaCompra.getText()) > 00)
            {
                try 
                {
                    String dniYNombre = cajaDNICompra.getSelectedItem().toString();
                    String[] dividoDNINombre = dniYNombre.split("_");
                    String dni = dividoDNINombre[0];
                    
                    String nombreProv = cajaIDPiezaCompra
                            .getSelectedItem().toString();                      //obtengo el nombre de la pieza

                    ResultSet rsIdPieza = gesConex.
                            obtenID_pieza(nombreProv);                          //obtengo Id pieza
                    String ID_pieza = "";
                    while (rsIdPieza.next())
                    {
                        ID_pieza = rsIdPieza.getString("ID_pieza");             //meto el ID del pieza en el String 
                    }
                    //----------------------------------------------------------
                    
                    String fecha_compra = cajaFechaCompra + "-" + cajaFechaCompra1 + "-" + cajaFechaCompra2;
                    
                    gesConex.insertaCompra(dni, ID_pieza, fecha_compra, cajaCantidadCCompra.getText());
                    
                    DefaultTableModel model = new DefaultTableModel();                  
                    tablaCompras.setModel(model);                               //Reiniciamos modelo de la tabla
                    hazTabla("cliente_pieza", model);
                }
                catch (SQLException ex) 
                {
                    System.out.println("ERROR: no se pudo hacer la compra");
                    ex.printStackTrace();
                }
            }
            else
            {
                String message = "No se puede hacer una compra de inferior a menos de 0 productos o en fechas inferieores a 1960-00-00";
                JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
                JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_botonInsertarCompraMousePressed
    //--------------------------------------------------------------------------
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VentanaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VentanaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VentanaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VentanaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new VentanaPrincipal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel apellido;
    private javax.swing.JButton botonBorrarCliente;
    private javax.swing.JButton botonBorrarPieza;
    private javax.swing.JButton botonBorrarProveedor;
    private javax.swing.JButton botonBuscarCliente;
    private javax.swing.JButton botonBuscarCompra;
    private javax.swing.JButton botonBuscarPieza;
    private javax.swing.JButton botonBuscarproveedor;
    private javax.swing.JToggleButton botonConexion;
    private javax.swing.JButton botonInsertarCliente;
    private javax.swing.JButton botonInsertarCompra;
    private javax.swing.JButton botonInsertarPieza;
    private javax.swing.JButton botonInsertarProveedor;
    private javax.swing.JTextField cajaApellido;
    private javax.swing.JTextField cajaBuquedaClientes;
    private javax.swing.JTextField cajaBuquedaCompra;
    private javax.swing.JTextField cajaBuquedaPieza;
    private javax.swing.JTextField cajaBuquedaProveedor;
    private javax.swing.JTextField cajaCantidadCCompra;
    private javax.swing.JTextField cajaCorreo;
    private javax.swing.JTextField cajaDNI;
    private javax.swing.JComboBox<String> cajaDNICompra;
    private javax.swing.JTextField cajaDescripcion;
    private javax.swing.JTextField cajaDireccion;
    private javax.swing.JTextField cajaDireccionProveedor;
    private javax.swing.JTextField cajaFabricante;
    private javax.swing.JTextField cajaFechaCompra;
    private javax.swing.JTextField cajaFechaCompra1;
    private javax.swing.JTextField cajaFechaCompra2;
    private javax.swing.JComboBox<String> cajaIDPiezaCompra;
    private javax.swing.JTextField cajaIdPieza;
    private javax.swing.JTextField cajaIdProveedor;
    private javax.swing.JTextField cajaNombre;
    private javax.swing.JTextField cajaNombrePieza;
    private javax.swing.JTextField cajaNombreProveedor;
    private javax.swing.JComboBox<String> cajaProveedorPieza;
    private javax.swing.JTextField cajaStock;
    private javax.swing.JLabel cantidad;
    private javax.swing.JComboBox<String> comboBoxClientes;
    private javax.swing.JComboBox<String> comboBoxCompra;
    private javax.swing.JComboBox<String> comboBoxPiezas;
    private javax.swing.JComboBox<String> comboBoxProveedor;
    private javax.swing.JLabel correo;
    private javax.swing.JLabel descricion;
    private javax.swing.JLabel direccion;
    private javax.swing.JLabel direcionProveedor;
    private javax.swing.JLabel dni;
    private javax.swing.JLabel dniCcompra;
    private javax.swing.JLabel fabricante;
    private javax.swing.JLabel fechaCompra;
    private javax.swing.JLabel fechaCompra1;
    private javax.swing.JLabel idPiezaCompra;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JToggleButton jToggleButton1;
    private javax.swing.JLabel nombre;
    private javax.swing.JLabel nombrePieza;
    private javax.swing.JLabel nombreProveedor;
    private javax.swing.JPanel panelClientes;
    private javax.swing.JPanel panelCompras;
    private javax.swing.JPanel panelConexion;
    private javax.swing.JPanel panelPiezas;
    private javax.swing.JPanel panelProveedores;
    private javax.swing.JLabel proveedor;
    private javax.swing.JLabel stock;
    private javax.swing.JTable tablaClientes;
    private javax.swing.JTable tablaCompras;
    private javax.swing.JTable tablaPiezas;
    private javax.swing.JTable tablaProveedores;
    // End of variables declaration//GEN-END:variables
}
